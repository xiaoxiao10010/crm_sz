package com.huike.clues.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecordDto {
    private Long clueId;  //线索id
    private String subject;//学科
    private String record;//根据记录
    private String level;//意向级别
    private String type;//0:正常跟进 1伪线索
    private String falseReason;//标记失败原因
    private String name;//姓名
    private String sex;// 性别
    private String weixin;//微信
    private String qq;//qq
    private Integer age;//年龄
}
