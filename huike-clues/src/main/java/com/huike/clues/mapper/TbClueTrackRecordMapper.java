package com.huike.clues.mapper;


import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.vo.ClueTrackRecordVo;
import com.huike.common.core.page.TableDataInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 线索跟进记录Mapper接口
 * @date 2021-04-19
 */
@Mapper
public interface TbClueTrackRecordMapper {




    @Insert("insert into tb_clue_track_record(clue_id,subject,record,level,type,false_reason,create_by,next_time)" +
            "values (#{clueTrackRecordVo.clueId},#{clueTrackRecordVo.subject},#{clueTrackRecordVo.record},#{clueTrackRecordVo.level},#{clueTrackRecordVo.type},#{clueTrackRecordVo.falseReason},#{username},#{clueTrackRecordVo.nextTime})")
    void add(@Param("clueTrackRecordVo") ClueTrackRecordVo clueTrackRecordVo, @Param("username") String username);

    void update(TbClue tbclue);




    @Select("select\n" +
            "       b.create_by,\n" +
            "       b.create_time,\n" +
            "       b.id,\n" +
            "       b.clue_id,\n" +
            "       b.subject,\n" +
            "       b.record,\n" +
            "       b.level,\n" +
            "       b.type,\n" +
            "       b.false_reason,\n" +
            "       b.next_time\n" +
            "from tb_clue_track_record b\n" +
            "where clue_id=#{clueId};")
    List<ClueTrackRecordVo> selectclueByid(Long clueId);



}
