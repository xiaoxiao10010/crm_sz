package com.huike.clues.service.impl;


import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.vo.ClueTrackRecordVo;
import com.huike.clues.mapper.TbClueTrackRecordMapper;
import com.huike.clues.service.ITbClueTrackRecordService;
import com.huike.common.core.domain.entity.SysUser;
import com.huike.common.core.page.TableDataInfo;
import com.huike.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 线索跟进记录Service业务层处理
 *
 * @date 2022-04-22
 */
@Service
public class TbClueTrackRecordServiceImpl implements ITbClueTrackRecordService {
    @Autowired
    private TbClueTrackRecordMapper tbClueTrackRecordMapper;

    @Override
    public void add(ClueTrackRecordVo tbClueTrackRecord) {
        TbClue tbclue = new TbClue();
        BeanUtils.copyProperties(tbClueTrackRecord, tbclue);

        tbclue.setId(tbClueTrackRecord.getClueId());
        tbClueTrackRecordMapper.update(tbclue);
        String username = SecurityUtils.getUsername();
        tbClueTrackRecordMapper.add(tbClueTrackRecord, username);
    }

    @Override
    public List<ClueTrackRecordVo> selectclue(Long clueId){
        List<ClueTrackRecordVo> tableDataInfo =
                tbClueTrackRecordMapper.selectclueByid(clueId);

        return tableDataInfo;
    }
}
