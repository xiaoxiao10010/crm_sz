package com.huike.business.mapper;

import java.util.List;
import com.huike.business.domain.TbBusinessTrackRecord;
import com.huike.business.domain.vo.BusinessTrackVo;
import org.apache.ibatis.annotations.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 商机跟进记录Mapper接口
 * @date 2021-04-28
 */
@Mapper
public interface TbBusinessTrackRecordMapper {

    List<TbBusinessTrackRecord> selectbusiness(Long id);

   @Insert("insert into tb_business_track_record " +
           "(business_id, create_by, key_items, record, create_time, track_status, next_time) VALUES " +
           "(#{businessTrackVo.businessId},#{username},#{businessTrackVo.keyItems},#{businessTrackVo.record},#{businessTrackVo.createTime},#{businessTrackVo.trackStatus},#{businessTrackVo.nextTime})")
    void addbusiness(@Param("businessTrackVo") BusinessTrackVo businessTrackVo,@Param("username") String username);
}