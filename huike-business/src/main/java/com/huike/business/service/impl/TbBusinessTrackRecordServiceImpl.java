package com.huike.business.service.impl;




import com.huike.business.domain.TbBusinessTrackRecord;
import com.huike.business.domain.vo.BusinessTrackVo;
import com.huike.business.mapper.TbBusinessTrackRecordMapper;
import com.huike.business.service.ITbBusinessTrackRecordService;
import com.huike.clues.mapper.SysDictDataMapper;
import com.huike.clues.service.impl.SysDictDataServiceImpl;
import com.huike.common.utils.SecurityUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * 商机跟进记录Service业务层处理
 * 
 * @author wgl
 * @date 2021-04-28
 */
@Service
public class TbBusinessTrackRecordServiceImpl implements ITbBusinessTrackRecordService {
   @Autowired
   private TbBusinessTrackRecordMapper tbBusinessTrackRecordMapper;

  @Autowired
   private SysDictDataServiceImpl sysDictDataService;


    @Override
    public void addbusiness(BusinessTrackVo businessTrackVo) {
        String username = SecurityUtils.getUsername();
       tbBusinessTrackRecordMapper.addbusiness(businessTrackVo,username);

    }

    @Override
    public List<TbBusinessTrackRecord> selectbusiness(Long id) {
        List<TbBusinessTrackRecord> tbBusinessTrackRecords = tbBusinessTrackRecordMapper.selectbusiness(id);
        for (TbBusinessTrackRecord tbBusinessTrackRecord : tbBusinessTrackRecords) {
            String keyItems=tbBusinessTrackRecord.getKeyItems();
            String[]split=keyItems.split(",");
            for (String keyValue : split) {
                String keyName=sysDictDataService.selectDictLabel("communication_point",keyValue);
                tbBusinessTrackRecord.getKeys().add(keyName);

            }
        }
        return  tbBusinessTrackRecords;
    }
}
