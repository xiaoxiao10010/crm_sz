package com.huike.business.service;


import com.huike.business.domain.TbBusinessTrackRecord;
import com.huike.business.domain.vo.BusinessTrackVo;
import org.springframework.stereotype.Service;

import java.util.List;
import com.huike.business.domain.TbBusinessTrackRecord;

import java.util.List;

/**
 * 商机跟进记录Service接口
 * @date 2021-04-28
 */
@Service
public interface ITbBusinessTrackRecordService {




    void addbusiness(BusinessTrackVo businessTrackVo);


    List<TbBusinessTrackRecord> selectbusiness(Long id);
}
