package com.huike.web.controller.report;


import com.huike.clues.domain.vo.ClueReportVo;
import com.huike.clues.service.ITbClueService;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.page.TableDataInfo;
import com.huike.report.domain.vo.LineChartVO;
import com.huike.report.domain.vo.SuoShuRenContractVO;
import com.huike.report.service.IReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.page.TableDataInfo;
import com.huike.report.domain.dto.PageDto;
import com.huike.report.domain.vo.*;
import com.huike.report.service.IReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.time.LocalDate;

import java.text.ParseException;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/report")
public class ReportController extends BaseController {
    @Autowired
    private IReportService iReportService;

    /**
     * 销售统计  时间列表
     *
     * @param begin
     * @param end
     * @return
     * @throws ParseException
     */
    @GetMapping("/salesStatistics/{begin}/{end}")
    public LineChartVO SalesStatistics(@PathVariable String begin, @PathVariable String end) throws ParseException {
        log.info("接收日期参数：{},{}", begin, end);
        LineChartVO lineChartVO = iReportService.salesStatistics(begin, end);
        return lineChartVO;
    }

    /**
     * 销售统计报表 --归属部门
     *
     * @param pageNum
     * @param pageSize
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/deptStatisticsList/{begin}/{end}")
    public TableDataInfo deptStatisticsList(Integer pageNum, Integer pageSize,
                                            @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate beginCreateTime,
                                            @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endCreateTime) {
        log.info("接收分页日期参数：{},{},{},{}", pageNum, pageSize, beginCreateTime, endCreateTime);
        TableDataInfo tableDataInfo = iReportService.deptStatisticsList(pageNum, pageSize, beginCreateTime, endCreateTime);
        return tableDataInfo;
    }

    /**
     * 销售统计报表 -- 归属人
     *
     * @param pageNum
     * @param pageSize
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/ownerShipStatisticsList/{beginCreateTime}/{endCreateTime}")
    public TableDataInfo ownerShipStatisticsList(Integer pageNum, Integer pageSize,
                                                 @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate beginCreateTime,
                                                 @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endCreateTime) {
        log.info("接收分页日期参数：{},{},{},{}", pageNum, pageSize, beginCreateTime, endCreateTime);
        TableDataInfo tableFateInfo = iReportService.ownerShipStatisticsList(pageNum, pageSize, beginCreateTime, endCreateTime);
        return tableFateInfo;
    }

    /**
     * 销售统计报表  归属渠道
     *
     * @param pageNum
     * @param pageSize
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/channelStatisticsList/{beginCreateTime}/{endCreateTime}")
    public TableDataInfo channelStatisticsList(Integer pageNum, Integer pageSize,
                                               @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate beginCreateTime,
                                               @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endCreateTime) {
        log.info("接收分页日期参数：{},{},{},{}", pageNum, pageSize, beginCreateTime, endCreateTime);
        TableDataInfo tableDataInfo = iReportService.channelStatisticsList(pageNum, pageSize, beginCreateTime, endCreateTime);
        return tableDataInfo;
    }


    /**
     * 渠道统计饼图
     * 线上活动  需要查看字典里面有关线上活动的数据
     * 然后在根据前台传的日期区间去活动表里去查看活动的数量以及渠道
     */
    @GetMapping("/chanelStatistics/{beginCreateTime}/{endCreateTime}")
    public List<ChanelAndNumVo> chanelStatistics(@PathVariable String beginCreateTime,
                                                 @PathVariable String endCreateTime) {
        log.info("接收参数,{},{}", beginCreateTime, endCreateTime);

        return iReportService.chanelStatistics(beginCreateTime, endCreateTime);
    }

    @GetMapping("/getVulnerabilityMap/{beginCreateTime}/{endCreateTime}")
    public AjaxResult reportloudou(@PathVariable String beginCreateTime,
                                   @PathVariable String endCreateTime) {
        VulnerabilityMapVo vulnerabilityMapVo = iReportService.reportloudou(beginCreateTime, endCreateTime);
        return AjaxResult.success(vulnerabilityMapVo);
    }

    /**
     * 活动统计饼图 统计的是已完成状态的  查询活动名称及数量 并且在合同表中是已完成状态
     *
     * @param beginCreateTime
     * @param endCreateTime   查询新增和总客户数
     * @param
     * @param
     * @return
     */
    @GetMapping("/activityStatistics/{beginCreateTime}/{endCreateTime}")
    public List<ActivityAndNameAndIdVo> activityStatistics(@PathVariable String beginCreateTime,
                                                           @PathVariable String endCreateTime) {
        log.info("接收参数,{},{}", beginCreateTime, endCreateTime);

        return iReportService.activityStatistics(beginCreateTime, endCreateTime);
    }

    @GetMapping("/contractStatistics/{beginCreateTime}/{endCreateTime}")
    public LineChartVO selectAllClient(@PathVariable String beginCreateTime, @PathVariable String endCreateTime) throws
            ParseException {
        LineChartVO lineChartVO = iReportService.selectAllClient(beginCreateTime, endCreateTime);
        return lineChartVO;
    }

    /**
     * 渠道统计报表
     *
     * @param tbActicitys 客户统计列表查询
     * @param
     * @return
     */
    @GetMapping("/activityStatisticsList")
    public TableDataInfo activityStatisticsList(TbActicitys tbActicitys) {
        //设置请求分页
        startPage();
        List<ActivityStatisticsVo> activityStatisticsList = iReportService.activityStatisticsList(tbActicitys);
        return getDataTable(activityStatisticsList);
    }

    @GetMapping("/contractStatisticsList")
    public TableDataInfo selectByLimit(PageDto pageDto) {
        TableDataInfo tableDataInfo = iReportService.selectByConditional(pageDto);
        return tableDataInfo;
    }

    /*线索统计新增*/


    @GetMapping("/subjectStatistics/{beginCreateTime}/{endCreateTime}")
    public List<ReportVo> subjectStatistics(@PathVariable String beginCreateTime, @PathVariable String endCreateTime) {
        return iReportService.subjectStatistics(beginCreateTime, endCreateTime);
    }


    @GetMapping("/cluesStatisticsList")
    public TableDataInfo cluesStatisticsList(String beginCreateTime, String endCreateTime) {
        startPage();
        List<ClueReportVo> clueReportVoList = iReportService.cluesStatisticsList(beginCreateTime, endCreateTime);
        return getDataTable(clueReportVoList);
    }

    @GetMapping("/cluesStatistics/{beginCreateTime}/{endCreateTime}")
    public LineChartVO cluesStatistics(@PathVariable String beginCreateTime,
                                       @PathVariable String endCreateTime) throws ParseException {
        log.info("时间参数:{},{}", beginCreateTime, endCreateTime);
        LineChartVO lineChartVO = iReportService.cluesStatistcsList(beginCreateTime, endCreateTime);
        return lineChartVO;
    }

}
