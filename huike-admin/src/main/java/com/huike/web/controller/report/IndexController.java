package com.huike.web.controller.report;


import com.huike.clues.domain.vo.IndexStatisticsVo;
import com.huike.common.core.domain.AjaxResult;

import com.huike.report.domain.vo.IndexVo;
import com.huike.report.service.IReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/index")
public class IndexController {
    @Autowired
    private IReportService iReportService;

    /**
     * 首页数据统计
     * @param endCreateTime
     * @return
     */
    @GetMapping
    public AjaxResult index(String beginCreateTime, String endCreateTime) {
        IndexVo indexVo = iReportService.index(beginCreateTime, endCreateTime);
        return AjaxResult.success(indexVo);
    }

    /**
     * 线索转换龙虎榜
     * @return
     */
    @GetMapping("/salesStatistic")
    public AjaxResult salesStatistic(IndexStatisticsVo statisticsVo) {
        statisticsVo.setBeginCreateTime(statisticsVo.getBeginCreateTime()+" 00:00:00");
        statisticsVo.setEndCreateTime(statisticsVo.getEndCreateTime()+" 23:59:59");

          List<Map<String,Object>> list= iReportService.salesStatistic(statisticsVo);

          return AjaxResult.success("成功",list);

    }
    /**
     * 线索转换龙虎榜
     * @return
     */
    @GetMapping("/businessChangeStatistics")
    public AjaxResult businessChangeStatistics(IndexStatisticsVo statisticsVo) {
        statisticsVo.setBeginCreateTime(statisticsVo.getBeginCreateTime()+" 00:00:00");
        statisticsVo.setEndCreateTime(statisticsVo.getEndCreateTime()+" 23:59:59");

        List<Map<String,Object>> list= iReportService.businessChangeStatistics(statisticsVo);

        return AjaxResult.success("成功",list);

    }


}