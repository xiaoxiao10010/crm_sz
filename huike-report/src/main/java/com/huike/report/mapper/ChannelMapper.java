package com.huike.report.mapper;

import com.huike.report.domain.vo.ActivityAndNameAndIdVo;
import com.huike.report.domain.vo.ActivityStatisticsVo;
import com.huike.report.domain.vo.TbActicitys;
import com.huike.report.domain.vo.sysDictDateVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@Mapper
public interface ChannelMapper {
   /* *
     * 查询字典包含线上活动的数据
     * @return
     */
    List<sysDictDateVo> selectDictDate();

    //设计多个表查询连接的时候 传入多个参数的时候需要用@Param注解去起一个别名
    List<ActivityAndNameAndIdVo> selectactivityStatistics(@Param("beginTime") LocalDateTime beginTime, @Param("endTime") LocalDateTime endTime);

    List<ActivityStatisticsVo> activityStatisticsList(TbActicitys tbActicitys);

}
