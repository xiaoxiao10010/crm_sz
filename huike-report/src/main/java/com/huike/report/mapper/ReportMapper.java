package com.huike.report.mapper;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.HashMap;
import java.util.List;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

import com.huike.report.domain.dto.ContractDto;

import com.huike.report.domain.vo.ChanelAndNumVo;
import com.huike.report.domain.vo.ConditionalQueryVo;
import org.apache.ibatis.annotations.Mapper;

import com.huike.report.domain.vo.ReportVo;

import com.github.pagehelper.Page;
import com.huike.report.domain.vo.ChannelStatisticsVO;
import com.huike.report.domain.vo.DeptContractVO;
import com.huike.report.domain.vo.SuoShuRenContractVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.huike.clues.domain.vo.IndexStatisticsVo;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 首页统计分析的Mapper
 *
 * @author Administrator
 */
@Mapper
public interface ReportMapper {
    /**=========================================基本数据========================================*/
    //查询新增用户数和总用户数
    Integer countByMap(HashMap<String, Object> map);

    //客户统计列表查询
    List<ConditionalQueryVo> selectByConditional(ContractDto contractDto);

    Integer getcluesNums(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime);


    Integer getEffectiveCluesNums(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime);

    Integer getBusinessNums(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime);

    Integer getContractNums(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime);

    Double getSalesAmount(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime);

    Integer getTofollowedCluesNum(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime);

    Integer getTofollowedBusinessNum(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime);

    Integer getToallocatedCluesNum(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime);

    Integer getToallocatedBusinessNum(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime);

    Integer getTodayCluesNum(@Param("beginTime") LocalDateTime beginTime, @Param("endTime") LocalDateTime endTime);

    Integer getTodayBusinessNum(@Param("beginTime") LocalDateTime beginTime, @Param("endTime") LocalDateTime endTime);


    Integer getTodayContractNum(@Param("beginTime") LocalDateTime beginTime, @Param("endTime") LocalDateTime endTime);

    Double getTodaySalesAmount(@Param("beginTime") LocalDateTime beginTime, @Param("endTime") LocalDateTime endTime);


    /**=========================================基本数据========================================*/
    /**
     * 销量统计报表 -- 归属部门
     * @param beginTime
     * @param endTime
     * @return
     */
    Page<DeptContractVO> deptStatisticsList(@Param("beginTime") LocalDateTime beginTime,@Param("endTime") LocalDateTime endTime);

    /**
     * 销量统计报表 -- 归属人
     * @param beginTime
     * @param endTime
     * @return
     */
    Page<SuoShuRenContractVO> ownerShipStatisticsList(@Param("beginTime") LocalDateTime beginTime,@Param("endTime") LocalDateTime endTime);

    /**
     * 销量统计报表 -- 归属渠道
     * @param beginTime
     * @param endTime
     * @return
     */
    Page<ChannelStatisticsVO> channelStatisticsList(@Param("beginTime") LocalDateTime beginTime, @Param("endTime") LocalDateTime endTime);
    /**=========================================基本数据========================================*/



    /**=========================================今日简报========================================*/
    /**=========================================今日简报========================================*/


    /**=========================================待办========================================*/



    List<ReportVo> subjectStatistics(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime);

    List<ChanelAndNumVo> chanelStatistics(@Param("beginTime")LocalDateTime beginTime, @Param("endTime")LocalDateTime endTime);
}
