package com.huike.report.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChannelStatisticsVO {

    private Double total_amount;
    private Integer num;
    private String channel;



}
