package com.huike.report.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class sysDictDateVo {
    private String dictLabel; //字典标签
    private String dictValue;  //字典键值
}
