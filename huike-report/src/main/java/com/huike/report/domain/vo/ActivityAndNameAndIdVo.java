package com.huike.report.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityAndNameAndIdVo {
    private Integer num;  //数量
    private Integer activityId;   //活动id
    private String activity;   //活动名称
}
