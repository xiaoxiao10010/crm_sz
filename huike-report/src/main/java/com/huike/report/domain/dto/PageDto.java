package com.huike.report.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class PageDto {
    /** 当前记录起始索引 */
    private Integer pageNum;

    /** 每页显示记录数 */
    private Integer pageSize;

    /** 开始时间 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate beginCreateTime;

    /** 结束时间 */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endCreateTime;

    /** 所属渠道ID */
    private String channel;

    /** 归属部门 */
    private Long deptId;

    /** 归属人 */
    private String createBy;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public LocalDate getBeginCreateTime() {
        return beginCreateTime;
    }

    public void setBeginCreateTime(LocalDate beginCreateTime) {
        this.beginCreateTime = beginCreateTime;
    }

    public LocalDate getEndCreateTime() {
        return endCreateTime;
    }

    public void setEndCreateTime(LocalDate endCreateTime) {
        this.endCreateTime = endCreateTime;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
}
