package com.huike.report.domain;

import com.huike.common.core.domain.BaseEntity;
import lombok.Data;

@Data
public class ClueChang extends BaseEntity {
    private String createBy;
    private String deptName;
    private Integer num;
    private Long deptId;
    private double radio;
}
