package com.huike.report.domain.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DeptContractVO {

    private String deptName;
    private Double total_amount;
    private Integer num;
    private Integer dept_id;


}
