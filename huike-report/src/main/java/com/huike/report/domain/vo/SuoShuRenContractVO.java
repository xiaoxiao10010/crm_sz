package com.huike.report.domain.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SuoShuRenContractVO {

    private String create_by;
    private Double total_amount;
    private Integer num;
}
