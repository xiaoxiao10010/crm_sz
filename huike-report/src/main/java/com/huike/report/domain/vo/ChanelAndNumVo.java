package com.huike.report.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChanelAndNumVo {
    private Integer num;   //数量
    private String channel;   //渠道
}
