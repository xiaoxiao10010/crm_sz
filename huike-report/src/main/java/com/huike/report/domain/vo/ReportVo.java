package com.huike.report.domain.vo;



import lombok.Data;

@Data
public class ReportVo {
    //学科
    private  String subject;
    //数量
    private Integer num;
}
