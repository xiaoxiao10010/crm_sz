package com.huike.report.service;

import java.text.ParseException;
import java.time.LocalDate;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import com.huike.common.core.page.TableDataInfo;
import com.huike.clues.domain.vo.ClueReportVo;
import com.huike.clues.domain.vo.IndexStatisticsVo;
import com.huike.common.core.page.TableDataInfo;
import com.huike.report.domain.dto.PageDto;
import com.huike.report.domain.vo.*;
import org.apache.ibatis.annotations.Param;

import com.huike.clues.domain.TbActivity;
import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.vo.IndexStatisticsVo;
import com.huike.contract.domain.TbContract;

public interface IReportService {

    /**
     * 销售统计 时间列表
     * @param begin
     * @param end
     * @return
     * @throws ParseException
     */
    LineChartVO salesStatistics(String begin, String end) throws ParseException;

    /**
     * 销售统计报表 -- 归属部门
     * @param pageNum
     * @param pageSize
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    TableDataInfo deptStatisticsList(Integer pageNum, Integer pageSize, LocalDate beginCreateTime, LocalDate endCreateTime);

    /**
     * 销售统计报表 -- 归属人
     * @param pageNum
     * @param pageSize
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    TableDataInfo ownerShipStatisticsList(Integer pageNum, Integer pageSize, LocalDate beginCreateTime, LocalDate endCreateTime);

    /**
     * 销售统计报表 -- 归属渠道
     * @param pageNum
     * @param pageSize
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    TableDataInfo channelStatisticsList(Integer pageNum, Integer pageSize, LocalDate beginCreateTime, LocalDate endCreateTime);
    List<ActivityStatisticsVo> activityStatisticsList(TbActicitys tbActicitys);

    List<ActivityAndNameAndIdVo> activityStatistics(String beginCreateTime, String endCreateTime);

    List<ChanelAndNumVo> chanelStatistics(String beginCreateTime, String endCreateTime);

    IndexVo index(String beginCreateTime, String endCreateTime);

    List<Map<String, Object>> salesStatistic(IndexStatisticsVo statisticsVo);

    List<Map<String, Object>> businessChangeStatistics(IndexStatisticsVo statisticsVo);

    VulnerabilityMapVo reportloudou(String beginCreateTime, String endCreateTime);

    LineChartVO selectAllClient(String beginCreateTime, String endCreateTime) throws ParseException;

    TableDataInfo selectByConditional(PageDto pageDto);

    List<ReportVo> subjectStatistics(String beginCreateTime, String endCreateTime);

    List<ClueReportVo> cluesStatisticsList(String beginCreateTime, String endCreateTime);

    LineChartVO cluesStatistcsList(String beginCreateTime, String endCreateTime) throws ParseException;
}
